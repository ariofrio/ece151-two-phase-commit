package com.andresriofrio.ece151.twophasecommit.util;

import java.util.concurrent.Callable;

public class Throwables {
    public static interface ExceptionWrapper<E> {
        E wrap(Exception e);
    }

    public static <T> T propagate(Callable<T> callable) throws RuntimeException {
        return propagate(callable, RuntimeException::new);
    }

    public static <T, E extends Throwable> T propagate(Callable<T> callable, ExceptionWrapper<E> wrapper) throws E {
        try {
            return callable.call();
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw wrapper.wrap(e);
        }
    }

    @FunctionalInterface
    public static interface RunnableWithException {
        public void run() throws Exception;
    }

    public static Thread newThread(RunnableWithException runnable) {
        return new Thread(() -> {
            propagate(() -> {
                runnable.run();
                return true;
            });
        });
    }
}

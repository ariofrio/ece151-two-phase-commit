package com.andresriofrio.ece151.twophasecommit.util;

public class Threads {
    public static Thread start(Runnable f) {
        Thread thread = new Thread(f);
        thread.start();
        return thread;
    }
    public static Thread startPropagated(Throwables.RunnableWithException f) {
        Thread thread = Throwables.newThread(f);
        thread.start();
        return thread;
    }
}

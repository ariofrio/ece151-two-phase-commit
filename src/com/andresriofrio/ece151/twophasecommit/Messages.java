package com.andresriofrio.ece151.twophasecommit;

import java.io.Serializable;

public class Messages {
    public static class VoteRequest implements Serializable {}
    public static class VoteCommit implements Serializable {}
    public static class VoteAbort implements Serializable {}
    public static class GlobalAbort implements Serializable {}
    public static class GlobalCommit implements Serializable {}

    public static class DecisionRequest implements Serializable {}
}

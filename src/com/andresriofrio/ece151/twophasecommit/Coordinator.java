package com.andresriofrio.ece151.twophasecommit;

import com.andresriofrio.ece151.twophasecommit.Messages.*;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class Coordinator {
    private final ExecutorService executor = Executors.newCachedThreadPool();
    private final List<ObjectOutputStream> outputStreams = new ArrayList<>();
    private final List<ObjectInputStream> inputStreams = new ArrayList<>();

    private final ServerSocket serverSocket;

    private boolean willTakeTooLong = false;
    private int willNotSendGlobalCommitToIndex = -1;
    public boolean getWillTakeTooLong() {
        return willTakeTooLong;
    }
    public void setWillTakeTooLong(boolean willTakeTooLong) {
        this.willTakeTooLong = willTakeTooLong;
    }
    public int getWillNotSendGlobalCommitToIndex() {
        return willNotSendGlobalCommitToIndex;
    }
    public void setWillNotSendGlobalCommitToIndex(int willNotSendGlobalCommitToIndex) {
        this.willNotSendGlobalCommitToIndex = willNotSendGlobalCommitToIndex;
    }

    public Coordinator(int port) throws IOException {
        serverSocket = new ServerSocket(port); // make sure to do before participants
    }

    public void acceptConnections(int participants) throws IOException {
        for (int i = 0; i < participants; i++) {
            Socket socket = serverSocket.accept();
            socket.setSoTimeout(5 * 1000); // in milliseconds
            outputStreams.add(new ObjectOutputStream(socket.getOutputStream()));
            inputStreams.add(new ObjectInputStream(socket.getInputStream()));
        }
    }

    // NOTE: Must not execute before all participants have connected!
    public void run() throws IOException, ClassNotFoundException, InterruptedException {
        if (willTakeTooLong) {
            Thread.sleep(10 * 1000);
        }

        log("sending VoteRequest to all participants");
        multicast(new VoteRequest());

        log("waiting for votes from each participant");
        boolean gotOneOrMoreAborts = false;
        for (ObjectInputStream inputStream : inputStreams) {
            Future<Object> future = executor.submit((Callable<Object>) inputStream::readObject);
            try {
                Object incoming = future.get();
                if (incoming instanceof VoteCommit) {
                    log("received message from participant: VoteCommit");
                    // nothing
                } else if (incoming instanceof VoteAbort) {
                    log("received message from participant: VoteAbort");
                    gotOneOrMoreAborts = true;
                } else {
                    throw new IllegalStateException();
                }
            } catch (ExecutionException e) {
                if (e.getCause() != null && e.getCause() instanceof SocketTimeoutException) {
                    log("timed out");
                    log("sending GlobalAbort to all participants");
                    multicast(new GlobalAbort());
                    return;
                }
            }
        }
        log("received votes from each participant");

        // broadcast result
        if (gotOneOrMoreAborts) {
            log("sending GlobalAbort to all participants");
            multicast(new GlobalAbort());
        } else {
            log("sending GlobalCommit to all participants");
            multicast(new GlobalCommit(), willNotSendGlobalCommitToIndex);
        }
    }

    private void log(Object o) {
        System.out.println("CORD: " + o.toString());
    }

    private void multicast(Object o) throws IOException {
        multicast(o, -1);
    }

    private void multicast(Object o, int willNotSendToIndex) throws IOException {
        int i = 0;
        for (ObjectOutputStream outputStream : outputStreams) {
            if (i++ == willNotSendToIndex) continue;
            outputStream.writeObject(o);
        }
    }

    public void close() throws IOException {
        serverSocket.close();
    }

}

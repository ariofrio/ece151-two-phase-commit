package com.andresriofrio.ece151.twophasecommit;

import com.andresriofrio.ece151.twophasecommit.util.Threads;
import com.andresriofrio.ece151.twophasecommit.Messages.*;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class Participant {
    public static final String LOCALHOST = "127.0.0.1";
    private final ObjectOutputStream coordinatorOutputStream;
    private final ObjectInputStream coordinatorInputStream;
    private final ServerSocket serverSocket;

    private final AtomicReference<Object> mostRecentLog = new AtomicReference<>(null);
    private final int localPort;

    private boolean willTakeTooLong = false;
    private boolean willVoteCommit = true;
    private List<ObjectOutputStream> peerOutputStreams = new ArrayList<>();
    private List<ObjectInputStream> peerInputStreams = new ArrayList<>();

    public boolean getWillTakeTooLong() {
        return willTakeTooLong;
    }
    public void setWillTakeTooLong(boolean willTakeTooLong) {
        this.willTakeTooLong = willTakeTooLong;
    }
    public boolean getWillVoteCommit() {
        return willVoteCommit;
    }
    public void setWillVoteCommit(boolean willVoteCommit) {
        this.willVoteCommit = willVoteCommit;
    }

    public Participant(int localPort, int coordinatorPort) throws IOException {
        Socket coordinatorSocket = new Socket(LOCALHOST, coordinatorPort);
        coordinatorSocket.setSoTimeout(5 * 1000); // in milliseconds
        coordinatorOutputStream = new ObjectOutputStream(coordinatorSocket.getOutputStream());
        coordinatorInputStream = new ObjectInputStream(coordinatorSocket.getInputStream());
        serverSocket = new ServerSocket(localPort);
        this.localPort = localPort;
    }

    public void acceptConnections(int participants) throws IOException {
        List<ObjectOutputStream> outputStreams = new ArrayList<>();
        List<ObjectInputStream> inputStreams = new ArrayList<>();
        for (int i = 0; i < participants; i++) { // TODO: this is a bug, this should be `participants - 1`
            Socket socket = serverSocket.accept();
            ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
            Threads.startPropagated(() -> {
                while (true) {
                    Object incoming = inputStream.readObject();
                    if (incoming instanceof DecisionRequest) {
                        log("received decision request from peer");
                        Object mostRecentLog = this.mostRecentLog.get();
                        if (mostRecentLog instanceof GlobalCommit) {
                            log("replying to decision request with: GlobalCommit");
                            outputStream.writeObject(new GlobalCommit());
                        } else if (mostRecentLog == null || mostRecentLog instanceof GlobalAbort) {
                            log("replying to decision request with: GlobalAbort");
                            outputStream.writeObject(new GlobalAbort());
                        } else {
                            // skip, we haven't received final decision either
                        }
                    } else {
                        throw new IllegalStateException();
                    }
                }
            });
        }
    }

    public void requestConnections(int[] participantPorts) throws IOException {
        for (int remotePort : participantPorts) {
            if (remotePort != localPort) {
                Socket peerSocket = new Socket("127.0.0.1", remotePort);
                peerOutputStreams.add(new ObjectOutputStream(peerSocket.getOutputStream()));
                peerInputStreams.add(new ObjectInputStream(peerSocket.getInputStream()));
            }
        }
    }

    public void run() throws IOException, ClassNotFoundException, InterruptedException {
        mostRecentLog.set(null);

        {
            log("waiting for vote request from coordinator");
            Object incoming;
            try {
                incoming = coordinatorInputStream.readObject();
            } catch (SocketTimeoutException e) {
                log("timed out");
                log("sending message to coordinator: VoteAbort");
                mostRecentLog.set(new VoteAbort());
                coordinatorOutputStream.writeObject(new VoteAbort());
                return; // TODO: Either locally abort, or receive the coordinator's GlobalAbort?
            }
            if (!(incoming instanceof VoteRequest)) {
                throw new IllegalStateException();
            }
            log("received vote request from coordinator");
        }

        if (willTakeTooLong) {
            Thread.sleep(10 * 1000);
        }

        if (willVoteCommit) {
            log("sending message to coordinator: VoteCommit");
            mostRecentLog.set(new VoteCommit());
            coordinatorOutputStream.writeObject(new VoteCommit());

            log("waiting for decision from coordinator");
            Object incoming;
            try {
                incoming = coordinatorInputStream.readObject();
            } catch (SocketTimeoutException e) {
                log("timed out");
                log("sending decision request to all peer participants");
                multicast(new DecisionRequest());
                // TODO: For a real implementation, this needs to listen to all peer participants at the same time,
                //       instead of one at a time. But for the demo, we just need one more peer, so this will work.
                for (ObjectInputStream peerInputStream : peerInputStreams) {
                    Object peerIncoming = peerInputStream.readObject();
                    if (peerIncoming instanceof GlobalCommit) {
                        log("received decision from peer participant: GlobalCommit");
                    } else if (peerIncoming instanceof GlobalAbort) {
                        log("received decision from peer participant: GlobalAbort");
                    } else {
                        throw new IllegalStateException();
                    }
                    mostRecentLog.set(peerIncoming);
                }
                return;
            }

            // update state with decision from coordinator
            if (incoming instanceof GlobalCommit) {
                log("received decision from coordinator: GlobalCommit");
            } else if (incoming instanceof GlobalAbort) {
                log("received decision from coordinator: GlobalAbort");
            } else {
                throw new IllegalStateException();
            }
            mostRecentLog.set(incoming);
        } else {
            log("sending message to coordinator: VoteAbort");
            mostRecentLog.set(new VoteAbort());
            coordinatorOutputStream.writeObject(new VoteAbort());
        }
    }

    private void multicast(Object o) throws IOException {
        for (ObjectOutputStream peerOutputStream : peerOutputStreams) {
            peerOutputStream.writeObject(o);
        }
    }

    private void log(Object s) {
        System.out.println(localPort + ": " + s.toString());
    }

    public void close() throws IOException {
        serverSocket.close();
    }

}

package com.andresriofrio.ece151.twophasecommit;

import com.andresriofrio.ece151.twophasecommit.util.Threads;

import java.io.IOException;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class Main {

    public static final int COORDINATOR_PORT = 8000;
    public static final int[] PARTICIPANT_PORTS = new int[]{8001, 8002};
    public static final String LOCALHOST = "127.0.0.1";

    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException, BrokenBarrierException {
//        runTrial(0, 0, false, -1);
//        runTrial(8001, 0, false, -1);
//        runTrial(0, 8001, false, -1);
//        runTrial(0, 0, true, -1);
        runTrial(0, 0, false, 0);
    }

    private static void runTrial(int willNotVoteCommitPort, int willTakeTooLongPort, boolean coordinatorWillTakeTooLong, int coordinatorWillNotSendGlobalCommitToIndex) throws IOException, ClassNotFoundException, InterruptedException, BrokenBarrierException {
        CyclicBarrier barrier = new CyclicBarrier(2 + PARTICIPANT_PORTS.length);

        Coordinator coordinator = new Coordinator(COORDINATOR_PORT);
        coordinator.setWillTakeTooLong(coordinatorWillTakeTooLong);
        coordinator.setWillNotSendGlobalCommitToIndex(coordinatorWillNotSendGlobalCommitToIndex);
        Threads.startPropagated(() -> {
            coordinator.acceptConnections(PARTICIPANT_PORTS.length);
            coordinator.run();
            barrier.await();
        });

        for (int localPort : PARTICIPANT_PORTS) {
            Participant participant = new Participant(localPort, COORDINATOR_PORT);
            if (localPort == willNotVoteCommitPort) {
                participant.setWillVoteCommit(false);
            }
            if (localPort == willTakeTooLongPort) {
                participant.setWillTakeTooLong(true);
            }
            Threads.startPropagated(() -> {
                participant.acceptConnections(PARTICIPANT_PORTS.length);
            });
            Threads.startPropagated(() -> {
                participant.requestConnections(PARTICIPANT_PORTS);
                participant.run();
                barrier.await();
            });
        }
        barrier.await();
    }
}
